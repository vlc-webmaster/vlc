<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Victory Life Church
 */

get_header(); ?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article <?php post_class(); ?>>
		
					<div class="entry-content">
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links above?', 'vlc' ); ?></p>
					
					</div><!-- .entry-content -->

				</article><!-- #post-## -->
				

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>