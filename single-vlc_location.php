<?php

get_header(); ?>


	<div id="primary" class="content-area">
    
				<div class="location-information-wrapper clearfix"><!-- This is location-specific informaiton like phone, address, and service times. Appears above each location page, below the header/hero image -->
                   <div class="container">
                        <div class="location-information">
                            <h2>Location Information</h2>
                                <div class="location-left">
                                    <div class="phone-number">
                                       <h3>Phone:</h3> 
                                       <div class="locat-info"><?php the_field('phone_number'); ?></div>
                                    </div>
                                    <div class="address">
                                       <h3>Address:</h3>
                                        <div class="locat-info">
                                            <?php the_field('street_address'); ?><br>
                                            <?php the_field('city_state'); ?> <?php the_field('zip_code'); ?>
                                        </div>
                                        <br />                    
                                        <div class="get_directions">
                                            <a class="video-link" href="<?php the_field('get_directions_link'); ?>" target="_blank">Get Directions <i class="fa fa-play"></i></a>
                                        </div>
                                    </div>
                                </div>

                            <?php if( have_rows('service_times') ): ?>

                            <div class="service-times location-right">
                                <h3>Services:</h3>
                                <?php while( have_rows('service_times') ) : the_row(); ?>
                                    
                                    <div class="service-info">
                                        <div class="service-day">
                                            <?php the_sub_field('service_day'); ?>
                                        </div>
                                        <div class="locat-info">
                                            <?php the_sub_field('service_time'); ?> Service
                                        </div>
                                    </div>
                                    
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
				</div>
    
	    <div class="container clearfix">
		    <main id="main" class="site-main"><!-- This is the beginning of the location page's main content -->
               
                <?php while ( have_posts() ) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <div class="entry-content">
                            

                            <?php the_content(); ?>


                        </div><!-- .entry-content -->

                    </article><!-- #post-## -->

                <?php endwhile; // end of the loop. ?>
            
		    </main><!-- #main -->
        </div>
	</div><!-- #primary -->


<?php // get_sidebar(); ?>
<?php get_footer(); ?>
