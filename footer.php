<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Victory Life Church
 */

$options = get_option( 'vlc_theme_options' );
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="join-us">
				Join us <a href="http://live.victorylifechurch.com/" target="_blank">Online</a>, or at a Victory Life Church <a href="<?php echo home_url(); ?>/locations">location near you</a>.
			</div><!-- .join-us -->
			<nav class="footer-menu">
				<ul>
					<li><a href="http://live.victorylifechurch.com/" target="_blank">Watch</a></li>
					<li><a href="<?php echo home_url(); ?>/locations">Locations</a></li>
					<li><a href="<?php echo home_url(); ?>/give">Give</a></li>
					<li><a href="<?php echo home_url(); ?>/contact">Contact Us</a></li>
				</ul>
			</nav><!-- .footer-menu -->
			<nav class="social-nav">
				<ul>
					<li><a href="<?php echo $options['tw_link']; ?>" target="_blank">
						<i class="fa fa-twitter"></i><span class="hidden-label">Follow us on Twitter</span>
					</a></li>
					<li>		
					<a href="<?php echo $options['fb_link']; ?>" target="_blank">
						<i class="fa fa-facebook"></i><span class="hidden-label">Like us on Facebook</span>
					</a></li>
					<li><a href="<?php echo $options['insta_link']; ?>" target="_blank">
						<i class="fa fa-instagram"></i><span class="hidden-label">Follow us on Instagram</span>
					</a></li> 
				</ul>
			</nav><!-- .social-nav -->
			<div class="site-info">
				<p class="copyright">&copy; <?php echo date('Y'); ?> Victory Life Church. All rights reserved.<br />
                    <a href="<?php echo home_url(); ?>/toc/">Terms & Conditions</a>  |  <a href="<?php echo home_url(); ?>/privacy-policy/">Privacy Policy</a>  |  <a href="<?php echo home_url(); ?>/return-exchange-policy/">Return & Exchange Policy</a>  |  <a href="<?php echo home_url(); ?>/charitable-contributions/">Charitable Contributions FAQ</a></p>
			</div><!-- .site-info -->
		</div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
