(function ($) {
    'use strict';
    /*global jQuery */
    /* jshint browser: true */
    
   
    
    $(window).load(function(){ // On load
		 $('.parallax-wrapper').height($('.parallax-scroll').outerHeight(true));
	});
	$(window).resize(function(){ // On resize
		 $('.parallax-wrapper').height($('.parallax-scroll').outerHeight(true));
	});
    
        
})(jQuery);

 