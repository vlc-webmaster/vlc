/*global jQuery */
/* jshint browser: true */

jQuery(document).ready(function ($) {
    
    'use strict';
    // global vars
    jQuery("#hero").height(jQuery(window).height());
    
    
        jQuery(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        jQuery('.invite-popup').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            jQuery('.invite-form').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });
    
        jQuery(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        jQuery('.download-pkg').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            jQuery('.download-pakages').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });   
    
    
        jQuery(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        jQuery('.share-popup').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            jQuery('.social-pakages').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });     
    
        jQuery(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        jQuery('.faq-popup').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            jQuery('.faq-block').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });   
    
        jQuery(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        jQuery('.contest-link').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            jQuery('.contest-details').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });     
    

});