(function ($) {
    'use strict';
    /*global jQuery */
    /* jshint browser: true */
    $(document).on('facetwp-loaded', function () {
           // toggle Life Group displayy
        $('.life-group-title').click(function (e) {
            $(this).next('.life-group-details').slideToggle();
        });
        
       // toggle Get Involved form display
        $('.lg-form-link').click(function (e) {
            $(this).parent().parent().find('.lg-form-container').slideToggle();
        });
    });
    
})(jQuery);