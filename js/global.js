/*global jQuery */
/* jshint browser: true */
jQuery(document).ready(function ($) {
    
    'use strict';
   

	$('.menu-toggle').click(function (e) {
		if ($(window).width() > 700) {
			$('.top-menu-wrap').slideToggle();
		} else {
			$('.general-nav').slideToggle();
		}

	});
    
    $('.widget-title').click(function (e) {
		if ($(window).width() < 700) {
			$(this).toggleClass('selected').next().slideToggle();
		}
	});

	$('a[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

    
    function appendMenu() {

        if ($(window).width() < 700) {
            $('.top-menu-content').appendTo('.general-nav');
        } else {
            $('.top-menu-content').appendTo('.top-menu-wrap');
        }
    }
    
    $(document).ready(function (e) {
        appendMenu();
    });

    $(window).resize(function (e) {
        appendMenu();
    });
    

	// toggle Get Involved form display
	$('.form-link').click(function (e) {
		$(this).parent().parent().find('.form-container').slideToggle();
	});
    

    
    
     // DOM Ready
    $(function () {

    // Binding a click event
    // From jQuery v.1.7.0 use .on() instead of .bind()
        $('.watch-video').bind('click', function (e) {

    // Prevents the default action to be triggered. 
            e.preventDefault();

    // Triggering bPopup when click event is fired
            $('#video-to-watch').bPopup({
                fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                followSpeed: 1500, //can be a string ('slow'/'fast') or int
                modalColor: '#00a8ff'
            });

        });

    });
	
});

