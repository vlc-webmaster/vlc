jQuery(window).load(function() {
    jQuery(document).on('facetwp-loaded', function () {    
      // MASSONRY Without jquery
      var container = document.querySelector('.blog-masonry-wrapper');
      var msnry = new Masonry( container, {
        itemSelector: '.older-posts',
        columnWidth: '.older-posts',                
      });
      var container = document.querySelector('.events-masonry-wrapper');
      var msnry = new Masonry( container, {
        itemSelector: '.related-post',
        columnWidth: '.related-post',                
      });       
    }); 
});
