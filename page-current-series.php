<?php
/*
Template Name: Current Series Page
*/

get_header(); ?>

	<div id="primary" class="content-area">
	    <div class="container">
		    <main id="main" class="site-main">
            
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content', 'page' ); ?>



                <?php endwhile; // end of the loop. ?>
            
		    </main><!-- #main -->
        </div>
	</div><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
