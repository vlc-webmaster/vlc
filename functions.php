<?php
/**
 * Victory Life Church functions and definitions
 *
 * @package Victory Life Church
 */


if ( ! function_exists( 'vlc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vlc_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Victory Life Church, use a find and replace
	 * to change 'vlc' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'vlc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'vlc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );


}
endif; // vlc_setup
add_action( 'after_setup_theme', 'vlc_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function vlc_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'vlc' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'vlc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vlc_scripts() {
	wp_enqueue_style( 'vlc-style', get_template_directory_uri() . '/css/global.css' );

	wp_enqueue_script( 'vlc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'vlc-script', get_template_directory_uri() . '/js/global.js', array('jquery', 'jquery-ui-tabs'), '43', true );
    
    wp_enqueue_script( 'vlc-life-group-script', get_template_directory_uri() . '/js/life-groups.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'vlc-bpopup', get_template_directory_uri() . '/js/bpopup.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'vlc-full-video', get_template_directory_uri() . '/js/full-video.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'vlc-modernizr', get_template_directory_uri() . '/js/modernizr-custom.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'vlc-blog-masonry', get_template_directory_uri() . '/js/masonry.js', array('masonry'), '', true );
    
    if( is_front_page() )
    {
    wp_enqueue_script( 'vlc-parallax-script', get_template_directory_uri() . '/js/parallax.js', array('jquery'), '', true );
    }
    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vlc_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Creative Navigation Admin functions
 */
require get_template_directory() . '/admin/admin.php';



/**
 * Remove WP Admin Bar.
 */

add_filter( 'show_admin_bar', '__return_false' );

/**
 * Display how many spots are left in the choice label when using the GP Limit Choices perk
 * http://gravitywiz.com/gravity-perks/
 */
add_filter( 'gplc_remove_choices', '__return_false' );
add_filter( 'gplc_pre_render_choice', 'my_add_how_many_left_message', 10, 4 );
function my_add_how_many_left_message( $choice, $exceeded_limit, $field, $form ) {
	$choice_counts = GWLimitChoices::get_choice_counts( $form['id'], $field );
	$count = rgar( $choice_counts, $choice['value'] ) ? rgar( $choice_counts, $choice['value'] ) : 0;
	$limit = rgar($choice, 'limit');
	$how_many_left = max( $limit - $count, 0 );
	$message = "($how_many_left spots left)";
	$choice['text'] = $choice['text'] . '<span style="color:#00a8ff;">' . " $message" . '</span>';
	return $choice;
}

/**
 * Custom admin styles.
 */

function endo_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/images/vlc-logo.png);
            background-size: 192px 192px;
            width: 192px;
            height: 192px;
        }
        body.login {
        	background: url(<?php echo get_template_directory_uri(); ?>/images/locations-header.jpg) top center no-repeat;
        	background-size: cover;
        }
        .login form {
        	background: #fff;
        }
        .login #nav a, .login #backtoblog a {
        	text-shadow: none;
        }
        .login #wp-submit {
        	background: #00A8FF;
			color: #fff;
        }
        .login #backtoblog a, .login #nav a {
        	color: #fff;
        }


    </style>
<?php }
add_action( 'login_enqueue_scripts', 'endo_login_logo' );

function endo_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'endo_login_logo_url' );

function endo_login_logo_url_title() {
    return get_bloginfo('name');
}
add_filter( 'login_headertitle', 'endo_login_logo_url_title' );

function vlc_disable_comment_url($fields) { 
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','vlc_disable_comment_url');