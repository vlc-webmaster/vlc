<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Victory Life Church
 */

$options = get_option( 'vlc_theme_options' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
	<div class="entry-content">
		<?php the_content(); ?>
	
	</div><!-- .entry-content -->

</article><!-- #post-## -->
