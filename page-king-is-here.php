<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:title" content="The King Is Here" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://victorylifechurch.com/wp-content/uploads/2015/11/KH-fb-share.png" />
<meta property="og:url" content="https://victorylifechurch.com/king-is-here/" />
<meta property="og:description" content="I'm excited to go to #thekingishere #comewithme #vlchurch" />
<meta property="og:site_name" content="The King Is Here" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<script type="text/javascript">
  (function() {
    var config = {
      kitId: 'opz0fcn'
    };
    var d = false;
    var tk = document.createElement('script');
    tk.src = '//use.typekit.net/' + config.kitId + '.js';
    tk.type = 'text/javascript';
    tk.async = 'true';
    tk.onload = tk.onreadystatechange = function() {
      var rs = this.readyState;
      if (d || rs && rs != 'complete' && rs != 'loaded') return;
      d = true;
      try { Typekit.load(config); } catch (e) {}
    };
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(tk, s);
  })();
</script>
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63639767-1', 'auto');
  ga('send', 'pageview');

</script>
<!--[if lt IE 9]>
<script>
	document.createElement('video');
</script>
<![endif]-->
</head>

<body class="tkih">
           
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5637e109370bba3e" async="async"></script>

   
    <div id="hero" class="promo">
        
        <video autoplay="autoplay" loop muted id="bgvid">
            <source src="<?php bloginfo('template_directory'); ?>/bgvideo/background-video.mp4" type="video/mp4">
            <source src="<?php bloginfo('template_directory'); ?>/bgvideo/background-video.ogv" type="video/ogg">
            <source src="<?php bloginfo('template_directory'); ?>/bgvideo/background-video.webm" type="video/webm">
            Your browser does not support the video tag.
        </video>
        <div class="center-div">
            <h1 class="aligncenter"><SPAN STYLE="font-style: italic;">#</SPAN>the<SPAN STYLE="font-family: 'Novecentosanswide-Medium'">king</SPAN>is<SPAN STYLE="font-family: 'Novecentosanswide-Medium'">here</SPAN></h1>
            <p class="aligncenter">Join us this season at any of our victory life church locations as we celebrate Christmas together.</p>
            <div class="aligncenter tkih-dates clearfix">
               <div class="clearfix">
                <div class="tkih-date full"><h3>Dec. 19 - 20</h3></div>
                </div>
             </div>
             <div class="cta-block">
            <h4 class="invite-popup button-white-alt tkih-cta">Send Invite</h4>
            <h4 class="faq-popup button-white-alt tkih-cta">Learn More</h4>
            </div>
            <div class="sat-warn aligncenter">Saturday Services are only at the <a href="<?php echo esc_url( home_url() ); ?>/sherman" target="_blank">Sherman Location.</a>  |  Click Here for <a class="contest-link" href="#">Contest Details</a></div>
            <div class="contest-details">

                <h4>NO PURCHASE OR PAYMENT OF ANY KIND IS NECESSARY TO ENTER OR WIN.</h4>

<p>The “#thekingishere card contest” is sponsored by Victory Life Church (“Sponsor”)This contest is governed by these official rules (“Official Rules”). By participating in the contest, each entrant agrees to abide by these Official Rules, including all eligibility requirements, and understands that the results of the contest, as determined by Sponsor and its agents, are final in all respects. The contest is subject to all federal, state and local laws and regulations and is void where prohibited by law.</p>

<p>This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook, Instagram, or Twitter. Any questions, comments or complaints regarding the promotion will be directed to Sponsor.</p>

                <h4>ELIGIBILITY</h4>
<p>The Contest is open to legal residents of their respective countries where not prohibited by law, who are eighteen (18) years of age or older at the time of entry who have Internet access and a valid e-mail account prior to the beginning of the Contest Period. Sponsor has the right to verify the eligibility of each entrant.</p>

<h4>SWEEPSTAKES PERIOD</h4>
<p>The Sweepstakes begins at Nov 20, 2015 Noon PST and ends at Dec 20, 2015, noon PST. (“Sweepstakes Period”). All entries (submissions) must be received on or before the time stated during that submission period. Sponsor reserves the right to extend or shorten the contest at their sole discretion.</p>
<h4>HOW TO ENTER</h4>
<p>You can enter the Sweepstakes by following the instructions printed on the back of the circle shaped cards. You can find the cards at Victory Life Church locations while supplies last. Entrants must do all required steps on the card and participate in the contest. After submitting the required information on the card, the entrant will receive one (1) entry into the drawing. The entrant must be able to be contacted through email or social messaging to receive the prize.
You may enter as many times as you wish, but only one entry will be counted.</p>

<h4>WINNER SELECTION</h4>
<p>All eligible entries received during the Submission Period will gathered into a database at the end of the Submission Period. A winner will be chosen at random.</p>
<p>The winners will be announced on or about Dec 31, 2015 on or about noon PST. Announcement and instructions for prize will be sent to the e-mail address or social media account supplied on the potential prize winner’s entry post. Each entrant is responsible for monitoring his/her e-mail account for prize notification and receipt or other communications related to this sweepstakes. If a potential prize winner cannot be reached by Administrator (or Sponsor) within fifteen (15) days, using the contact information provided at the time of entry, or if the prize is returned as undeliverable, that potential prize winner shall forfeit the prize. Upon the request of the Sponsor, the potential winner may be required to return an Affidavit of Eligibility, Release and Prize Acceptance Form and IRS W-9 form. If a potential winner fails to comply with these official rules, that potential winner will be disqualified. Prizes may not be awarded if an insufficient number of eligible entries are received.</p>

<h4>PRIZES:</h4>
<p>Grand Prize: One Gift card for Amazon.com valued at $50.</p>
                <p>Terms and conditions may apply. Incidental expenses and all other costs and expenses which are not specifically listed as part of a prize in these Official Rules and which may be associated with the award, acceptance, receipt and use of all or any portion of the awarded prize are solely the responsibility of the respective prize winner. <strong>ALL FEDERAL, STATE AND LOCAL TAXES ASSOCIATED WITH THE RECEIPT OR USE OF ANY PRIZE IS SOLELY THE RESPONSIBILITY OF THE WINNER.</strong></p>
                
<h4>ADDITIONAL LIMITATIONS</h4>
<p>Prize is non-transferable. No substitution or cash equivalent of prizes is permitted. Sponsor and its respective parent, affiliate and subsidiary companies, agents, and representatives are not responsible for any typographical or other errors in the offer or administration of the Sweepstakes, including, but not limited to, errors in any printing or posting or these Official Rules, the selection and announcement of any winner, or the distribution of any prize. Any attempt to damage the content or operation of this Sweepstakes is unlawful and subject to possible legal action by Sponsor. Sponsor reserves the right to terminate, suspend or amend the Sweepstakes, without notice, and for any reason, including, without limitation, if Sponsor determines that the Sweepstakes cannot be conducted as planned or should a virus, bug, tampering or unauthorized intervention, technical failure or other cause beyond Sponsor’s control corrupt the administration, security, fairness, integrity or proper play of the Sweepstakes. In the event any tampering or unauthorized intervention may have occurred, Sponsor reserves the right to void suspect entries at issue. Sponsor and its respective parent, affiliate and subsidiary companies, agents, and representatives, and any telephone network or service providers, are not responsible for incorrect or inaccurate transcription of entry information, or for any human error, technical malfunction, lost or delayed data transmission, omission, interruption, deletion, line failure or malfunction of any telephone network, computer equipment or software, the inability to access any website or online service or any other error, human or otherwise.</p>

<h4>INDEMNIFICATION AND LIMITATION OF LIABILITY</h4>
    <p><strong>BY ENTERING THE SWEEPSTAKES, EACH ENTRANT AGREES TO INDEMNIFY, RELEASE AND HOLD HARMLESS SPONSOR AND ITS PARENT, AFFILIATE AND SUBSIDIARY COMPANIES, THE FACEBOOK PLATFORM, THE INSTAGRAM PLATFORM, THE TWITTER PLATFORM, ADMINISTRATOR, ADVERTISING AND PROMOTIONAL AGENCIES, AND ALL THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES AND AGENTS FROM ANY LIABILITY, DAMAGES, LOSSES OR INJURY RESULTING IN WHOLE OR IN PART, DIRECTLY OR INDIRECTLY, FROM THAT ENTRANT’S PARTICIPATION IN THE SWEEPSTAKES AND THE ACCEPTANCE, USE OR MISUSE OF ANY PRIZE THAT MAY BE WON. SPONSOR AND ITS PARENT, AFFILIATE AND SUBSIDIARY COMPANIES DO NOT MAKE ANY WARRANTIES, EXPRESS OR IMPLIED, AS TO THE CONDITION, FITNESS OR MERCHANTABILITY OF THE PRIZE. SPONSOR AND ITS PARENTS, SUBSIDIARIES, AFFILIATES, ADVERTISING AND PROMOTIONAL AGENCIES, AND ALL THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES AND AGENTS DISCLAIM ANY LIABILITY FOR DAMAGE TO ANY COMPUTER SYSTEM RESULTING FROM ACCESS TO OR THE DOWNLOAD OF INFORMATION OR MATERIALS CONNECTED WITH THE SWEEPSTAKES.</strong></p>

<h4>PUBLICITY</h4>
<p>By participating, each entrant grants Sponsor permission to use his/her name, likeness or comments for publicity purposes without payment of additional consideration, except where prohibited by law.</p>
<h4>SWEEPSTAKES SPONSORS</h4>
This sweepstakes is sponsored by:<br />
Victory Life Church<br />
PO Box 427<br />
Durant OK, 74702
           
           
            </div>
            
            <div class="faq-block">


<h4>What is it?</h4>
<p>In our weekend services, for the 4 weeks leading up to Christmas, We are taking a deeper look at the Kingship of Jesus. What does it mean for Him to be “Here” in our lives today? </p> 
<p>We will be looking at the Christmas story in a fresh way. What was it like before He came? Why was there such opposition to His coming? What did He do when He was here and why should it matter? How does Him being “King” mean anything in the crazy world we live in today? We hope to answer these questions.</p> 

<h4>Where is it?</h4>
                <p>We will be celebrating the Christmas story in all of our nine victory life Church locations. <a href="<?php echo esc_url( home_url() ); ?>/locations" target="_blank">Visit this page</a> to find a location near you.</p> 

<h4>When is it?</h4>
<p>"The King is Here" begins the weekend of november 28th and 29th and continues for 3 more weekends after that. Each service will be something special so you don’t want to miss any of them. The final week (Dec. 19-20th) will be a Huge Christmas celebration service that will be tons of fun for the entire family.</p> 

<h4>Why should I come and invite my friends?</h4>
<p>Because this REALLY matters. No one should live their life without hope. It doesn’t matter who you are, what you’ve done, or where you’re coming from. Jesus cares for you and He offers hope. There is no darkness so great that He can’t help bring you through it. You should come. You are welcome here.
                </p>                
                                                
            </div>
            <div class="invite-form">
                <h3 class="aligncenter">Invite your Friends or Family!</h3>
                <?php gravity_form('The King Is here Share Box', false, false, false, '', true, 12); ?>
            </div>
            <div class="download-pakages">
             <div class="dpleft">
                 <iframe src="https://player.vimeo.com/video/146309240" width="600" height="337" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
             </div>
              <div class="dpright">
               <p>Download Packages for Children</p>
               <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/nativity-set.jpg" alt="Nativity Craft Set" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/Nativity-Craft-Print-Set-With-Instructions.zip" target="_blank">Nativity Craft Set with instructions</a>
                   </div>
                   <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/crown-craft-set.jpg" alt="Crown Craft Set" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/Crown-Craft-Set.zip" target="_blank">Crown Craft Set</a>
                   </div>
                   <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/activity-pages.jpg" alt="Activity Pages" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/Activity-Pages.zip" target="_blank">Activity Pages</a>
               </div>
            </div>
            </div>
            <div class="social-pakages">
               <p>Download Packages for Social Media and Invites</p>
                   <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/TKH-fb-thumb.jpg" alt="Facebook Profile Pic" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/TKH-fb-profile.jpg" target="_blank">Facebook Profile Pic</a>
                   </div>
                   <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/TKH-fb-thumb.jpg" alt="Facebook Cover Photo" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/KH-fb-cover.png" target="_blank">Facebook Cover Photo</a>
                   </div>
                   <div class="link-images">
                   <img src="<?php bloginfo('template_directory'); ?>/downloads/TKH-invite-thumb.jpg" alt="Activity Pages" />
                   <a href="<?php bloginfo('template_directory'); ?>/downloads/TKH-invite-4x6.zip" target="_blank">Printable Invite Cards</a>
               </div>
            </div>
        </div>
        
        <button id="vidpause" class="play-pause"><i class="fa fa-pause"></i></button>
        <div class="site-branding">
				<h1 class="site-title">
					<a class="logo" href="<?php echo esc_url( home_url() ); ?>" rel="home">Victory Life Church</a>		
				</h1>
        </div><!-- .site-branding -->
        
        <div class="promo-items">
            <ul>   
                <a href="<?php echo esc_url( home_url() ); ?>/current-series" target="_blank"><li>View Sermons</li></a>            
                <a href="<?php echo esc_url( home_url() ); ?>/locations" target="_blank"><li>Locations</li></a>
                <li class="share-popup">Spread the Word</li>
                <a href="https://www.juicer.io/thekingishere" target="_blank"><li>Social Wall</li></a>
                <li class="download-pkg last">Kids Activities</li>
            </ul>
        </div>
    </div>
    

<script>
    
var vid = document.getElementById("bgvid"),
pauseButton = document.getElementById("vidpause");
function vidFade() {
    vid.classList.add("stopfade");
}
vid.addEventListener('ended', function() {
    // only functional if "loop" is removed 
     vid.pause();
	// to capture IE10
	vidFade();
});
pauseButton.addEventListener("click", function() {
    vid.classList.toggle("stopfade");
	if (vid.paused) {
vid.play();
		pauseButton.innerHTML = "<i class=\"fa fa-pause\"></i>";
	} else {
        vid.pause();
        pauseButton.innerHTML = "<i class=\"fa fa-play\"></i>";
	}
})   
    
var addthis_share = addthis_share || {}
addthis_share = {
	passthrough : {
		twitter: {
			via: "vlchurch",
			text: "I'm excited to go to #thekingishere #comewithme #vlchurch"
		}
    }
}

</script>

<?php wp_footer(); ?>

</body>
</html>