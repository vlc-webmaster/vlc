<?php

// Display options
// $options = get_option( 'vlc_theme_options' );
// echo $options['tw_link'];

// add theme options page
add_action( 'admin_menu', 'vlc_theme_options' );

function vlc_theme_options() {

	add_theme_page( 'Theme Options', 'Theme Options', 'administrator', 'theme-options', 'vlc_theme_options_page' );

}

function vlc_theme_options_page() {
	?>
	<div class="wrap">
		<h2>Theme Options</h2>

		<form method="post" enctype="multipart/form-data" action="options.php">
        <?php 

          settings_fields('vlc_theme_options'); 
        
          do_settings_sections('vlc_theme_options.php');
        ?>
            <p class="submit">  
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
            </p>  
            
      </form>

	</div>
	<?php 
}

/**
 * Register the settings to use on the theme options page
 */
add_action( 'admin_init', 'vlc_register_settings' );

/**
 * Function to register the settings
 */
function vlc_register_settings() {

	// Register the settings with Validation callback
    register_setting( 'vlc_theme_options', 'vlc_theme_options', 'vlc_validate_settings' );

    // Add settings section
    add_settings_section( 'vlc_social_section', '', 'vlc_display_social_section', 'vlc_theme_options.php' );

    
    // create settings fields

    

    // Create facebook field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'fb_link',
      'name'      => 'fb_link',
      'desc'      => 'Full URL to Facebook page',
      'std'       => '',
      'label_for' => 'fb_link',
      'class'     => ''
    );

    add_settings_field( 'fb_link', 'Facebook URL', 'vlc_display_setting', 'vlc_theme_options.php', 'vlc_social_section', $field_args );

    // Create twitter field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'tw_link',
      'name'      => 'tw_link',
      'desc'      => 'Full URL to Twitter page',
      'std'       => '',
      'label_for' => 'tw_link',
      'class'     => ''
    );

    add_settings_field( 'tw_link', 'Twitter URL', 'vlc_display_setting', 'vlc_theme_options.php', 'vlc_social_section', $field_args );

    // Create instagram field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'insta_link',
      'name'      => 'insta_link',
      'desc'      => 'Full URL to Instagram page',
      'std'       => '',
      'label_for' => 'insta_link',
      'class'     => ''
    );

    add_settings_field( 'insta_link', 'Instagram URL', 'vlc_display_setting', 'vlc_theme_options.php', 'vlc_social_section', $field_args );


}

/**
 * Function to add extra text to display on each section
 */


function vlc_display_social_section( $section ){ 
	echo '<h3 class="title">Social Links</h3>';
}



/**
 * Function to display the settings on the page
 * This is setup to be expandable by using a switch on the type variable.
 * In future you can add multiple types to be display from this function,
 * Such as checkboxes, select boxes, file upload boxes etc.
 */
function vlc_display_setting($args)
{
    extract( $args );

    $option_name = 'vlc_theme_options';

    $options = get_option( $option_name );

    switch ( $type ) {  
          case 'text':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' />";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;  

          case 'textarea':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              echo "<textarea class='large-text $class' id='$id' name='" . $option_name . "[$id]'>$options[$id]</textarea>";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;  

          case 'checkbox':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              echo "<input class='$class' type='checkbox' id='$id' name='" . $option_name . "[$id]' " . checked( $options[$id], 'on', false ) . " />";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;  

          case 'radio':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              $values = explode( ', ', $values );
             	
              echo '<fieldset><p>';
              foreach ( $values as $value ) {
              	
              	 echo '<label><input type="radio" name=' . $option_name . '[' . $id . ']' . ' value="' . $value . '" ' . checked( $options[$id], $value, false ) . '/>' . $value . '</label><br>';
              }
              echo '</p></fieldset>';
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;  

          case 'select':  
              $options[$id] = stripslashes($options[$id]);  
              $options[$id] = esc_attr( $options[$id]);  
              $values = explode( ', ', $values );
              echo '<select name="' . $option_name . '[' . $id . ']' . '" id="' . $id . '">';
              foreach ( $values as $value ) {
              	
              	 echo '<option value="' . $value . '" ' . selected( $options[$name], $value, false) . '>' . $value . '</option>';
              }
              echo '</select>';
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;  
    }
}

/**
 * Callback function to the register_settings function will pass through an input variable
 * You can then validate the values and the return variable will be the values stored in the database.
 */
function vlc_validate_settings($input) {

  foreach($input as $k => $v) {
    $newinput[$k] = trim($v);
    
    // Check the input is a letter or a number
    // if(!preg_match('/^[A-Z0-9 _]*$/i', $v)) {
    //   $newinput[$k] = '';
    // }
  }

  return $newinput;
}