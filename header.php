<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Victory Life Church
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<script>
  (function(d) {
    var config = {
      kitId: 'hts3qoy',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63639767-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
    
      <?php 

		$options = get_option( 'vlc_theme_options' );

		if ( has_post_thumbnail() && !is_archive() ) {
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
			$thumb_url = $thumb_url_array[0];

			$style = 'background-image: url(' . $thumb_url . ');';
		} else {
			$style = "";
		}
	?>    <!-- Sets up the Featured image as a background image element in the div below -->      

	<header id="masthead" class="site-header" style="<?php echo $style; ?>">
		
            <div class="nav-outter-wrapper clearfix"><!-- Sets up the outter wrapper of the nav element, for full-width background-color styling -->
            
            <div class="container"><!-- Sets the inner menu width to 1200 max and auto margins for centering -->

			<div class="site-branding"><!-- This section is used in conjuction with the site title to created the left-aligned logo -->
				<h1 class="site-title">
					<a class="logo" href="<?php echo esc_url( home_url() ); ?>" rel="home">Victory Life Church</a>		
				</h1>
			</div><!-- .site-branding -->

			<div class="site-navigation">
				
				<nav class="general-nav"><!-- The main, always visible menu, only add very important links to this menu. If not supper important, add it to one of the drop-down menus (contact, about, content) -->
					<ul>
						<li><a href="http://live.victorylifechurch.com/" target="_blank">Live</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/current-series">Current Series</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/who-we-are">Who We Are</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/locations">Locations</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/give">Give</a></li>
						<li class="menu-toggle-li"><a class="menu-toggle">Menu <i class="fa fa-bars"></i></a></li>
					</ul>
				</nav>
				
				<a class="menu-toggle menu-toggle-mobile">Menu <i class="fa fa-bars"></i></a> <!-- Menu toggle while in Mobile -->
				
                <div class="top-menu-wrap"><!-- Permanently hidden menu that only displays on toggle, for more general links that dont need to be on the main nav above -->
                    <div class="top-menu-content">
                        <div class="column">
                            <h3 class="widget-title">About</h3><!-- This will display all links in the About menu -->
                            <?php wp_nav_menu( array( 'menu' => 'About', 'container' => '' ) ); ?>
                        </div>
                        <div class="column">
                            <h3 class="widget-title">Connect</h3><!-- This will display all links in the Connect menu -->
                            <?php wp_nav_menu( array( 'menu' => 'Connect', 'container' => '' ) ); ?>
                        </div>
                        <div class="column"><!-- This will display all locations pages as a clickable link to the page, it will automatically update if you add a new Location under the WP-admin area -->
                            <h3 class="widget-title">Locations</h3><!-- The following php code sets up the query for the vlc_location post type to be used in the menu display -->
                                <?php 
                                    $locations_menu_args = array(
                                        'post_type' => 'vlc_location',
                                        'orderby' => 'title',
                                        'order' => 'ASC',
                                    );

                                    $locations_menu_query = new WP_Query( $locations_menu_args );

                                    if( $locations_menu_query->have_posts() ) :

                                ?>
                                <ul class="menu"><!-- This states that while the query above brings back posts, take the post and display its title and wrapp in in the posts' permalink -->

                                    <?php while ( $locations_menu_query->have_posts() ) : $locations_menu_query->the_post(); ?>

                                    <li><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>

                                    <?php endwhile; ?>

                                </ul>
                                <?php

                                    endif;
                                    wp_reset_postdata();

                                ?>
                        </div>
                        <div class="column last">
                            <h3 class="widget-title">Content</h3><!-- This will display all links in the Content menu -->
                            <?php wp_nav_menu( array( 'menu' => 'Content', 'container' => '' ) ); ?>
                        </div>
                    </div>
                </div>				
				
			</div> <!-- .site-navigation -->

</div><!-- .container -->
</div>
            
            <div class="hero-content clearfix">
               <div class="outer-wrapper">
                <div class="container clearfix">    
                     <div class="intro clearfix"> 
                       <?php if ( is_404() ) { ?><!-- 404 Hero Image Content -->
                           
                            <h2 style="color:#313131;">Sorry, that page can't be found.</h2>
                            <h3 style="color:#313131;">Please try one of the links above.</h3>
                            
                        <?php } else if ( get_post_type() == 'post' && !is_archive() ) { ?>
                            
                            <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>               
                            
                        <?php } else if ( is_front_page() ) { ?><!-- Front Page Hero Image Content -->
                           
                            <img class="home-hero-img" src="<?php the_field('series_image'); ?>" alt="<?php the_field('series_image_alt_text'); ?>" />
                            <a class="video-link" href="<?php echo esc_url( home_url() ); ?>/current-series">View Series <i class="fa fa-play"></i></a>
                            
                       <?php } else if (  is_page_template( 'page-give.php' ) ) { ?><!-- Give Page Hero Image Content -->
                           
                            <h2><?php the_field( 'hero_title' ); ?></h2>
						    <h3><?php the_field( 'hero_subtitle' ); ?></h3>
                            <?php if ( get_field( 'sign_in_link' ) ) { ?>
                                <div class="give-button"><a href="<?php the_field('sign_in_link'); ?>" class="button-white-alt aligncenter">Sign in to Give</a></div>
                            <?php } if ( get_field( 'anonymous_link' ) ) { ?>
                                <div class="give-button"><a href="<?php the_field('anonymous_link'); ?>" class="button-white-alt aligncenter">Give Anonymously</a></div>
                            <?php } if ( get_field( 'other_ways_link' ) ) { ?>
                                <a href="<?php the_field('other_ways_link'); ?>" class="other-ways-to-give">See other ways to give <i class="fa fa-play"></i></a>
                            <?php } ?>
                            
                        <?php } elseif ( is_page_template('page-current-series.php') ) {?><!-- Current Series Page Hero Content -->

                            <div class="current-video">
                                <?php the_field( 'video_link' ); ?>
                                <h3><?php the_field( 'latest_message_title' ) ?></h3>
                            </div>    
                            
                        <?php } elseif ( is_page_template('page-locations.php') ) { ?><!-- This block of code sets up a locations list on the /locations page similar to the menu setup in the nav above. -->

                            <h3 class="locations">Find a location near you.</h3>
                            <?php
                            $locations_args = array(
                                'post_type' => 'vlc_location',
                                'orderby' => 'title',
                                'order' => 'ASC',
                            );
                            $locationsQuery = new WP_Query( $locations_args );
                            if( $locationsQuery->have_posts() ) :
                            ?>
                            <div class="locations-list">
                                <?php while ( $locationsQuery->have_posts() ) : $locationsQuery->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" class="location-name"><?php the_field('city_state') ?></a>
                                <?php endwhile; ?>
                            </div><!-- .locations-list -->
                            <?php endif; wp_reset_postdata(); ?>	

                        <?php } elseif ( get_field( 'show_link' ) == 'Yes') { ?><!-- if a post/page has a video to show -->
                            <h4 class="video-link watch-video">Watch Video <i class="fa fa-play"></i></h4>
                            <div id="video-to-watch">
                                <?php the_field( 'video_link' ); ?>
                            </div>    
                            
                        <?php } else { ?><!-- All Other Pages' Hero Image Content -->
                            <?php if ( get_field( 'hero_title' ) ) { ?><!-- This is set to only show the Hero Title if the field exists for the page -->
                                <h2><?php the_field( 'hero_title' ); ?></h2>
                            <?php } if ( get_field( 'hero_subtitle' ) ) { ?><!-- This is set to only show the Hero Subtitle if the field exists for the page -->    
                                <h3><?php the_field( 'hero_subtitle' ); ?></h3>    
                            <?php } ?>   
                        <?php } ?>
                    </div>        
                </div>
            </div>
			</div><!-- .hero-content -->
			
	</header><!-- #masthead -->

	<div id="content" class="site-content">
