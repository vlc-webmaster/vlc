<?php
/*
Template Name: Home Page
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); 

	if($post->post_content!="") {
	?>

	<div id="primary" class="content-area">
	    <div class="container">
		    <main id="main" class="site-main">
               
                <?php // while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content', 'page' ); ?>



                <?php // endwhile; // end of the loop. ?>
            
		    </main><!-- #main -->
        </div>
	</div><!-- #primary -->

	<?php }
	endwhile; // end of the loop. ?>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
